import { Router } from 'express'
import { randomBytes } from 'crypto'
import { Todo } from '../models/Todo'
import { type } from 'os'

type RequestBody = {
    title: string
}
type RequestParams = {
    id: string
}

let todos: Todo[] = []
const router = Router()

router.get('/', )

router.post('/todo', (req, res, next) => {
    const body = req.body as RequestBody

    const newTodo: Todo = {
        id: randomBytes(3).toString(),
        title: body.title
    }

    todos.push(newTodo)
    res.status(201).json({ message: 'Todo created', newTodo: newTodo })

})


router.put('/todo/:id', (req, res, next) => {
    const params = req.params as RequestParams
    const body = req.body as RequestBody

    const todoIndex = todos.findIndex(t => t.id === params.id)
    if (todoIndex >= 0) {
        todos[todoIndex] = {
            id: todos[todoIndex].id,
            title: body.title
        }

        res.status(200).json({ message: 'Todo updated', todos: todos })
    }
    res.status(404).json({ message: 'Not found' })
})

router.delete('/todo/:id', (req, res, next) => {
    const id = req.params.id
    todos.filter(t => t.id !== id)

    res.status(200).json({ message: 'Todo deleted', todos: todos })
})

export default router